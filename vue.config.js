// vue.config.js
module.exports = {
     publicPath: process.env.NODE_ENV === 'production'
    ? '/hello-world/'
    : '/',
  // 去掉文件名中的 hash
  filenameHashing: false,
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      // 为生产环境修改配置...
    } else {
      // 为开发环境修改配置...
    }
  },
  chainWebpack: config => {
    // 不生成 index,删除 HTML 相关的 webpack 插件
    // config.plugins.delete('html')
    // config.plugins.delete('preload')
    // config.plugins.delete('prefetch')
    // ========================================
    // 移除 prefetch 插件
    // config.plugins.delete('prefetch')
    // 当 prefetch 插件被禁用时，你可以通过 webpack 的内联注释手动选定要提前获取的代码区块：
    // import(/* webpackPrefetch: true */ './someAsyncComponent.vue')
    // 或者
    // 修改它的选项：
    // config.plugin('prefetch').tap(options => {
    //   options[0].fileBlacklist = options[0].fileBlacklist || []
    //   options[0].fileBlacklist.push(/myasyncRoute(.)+?\.js$/)
    //   return options
    // })
    // 内联文件(images)限制设置为 10kb
    // <img src="./image.png"> =>> h('img', { attrs: { src: require('./image.png') }})
    // config.module
    //   .rule('images')
    //     .use('url-loader')
    //       .loader('url-loader')
    //       .tap(options => Object.assign(options, { limit: 10240 }))
      output: {
        libraryExport: 'default'
      }
     // config.module
     //  .rule('vue')
     //  .use('vue-loader')
     //    .loader('vue-loader')
     //    .tap(options => {
     //      // 修改它的选项...
     //      return options
     //    })
     //    链式操作 (高级)
     // config.module
     //  .rule('vue')
     //  .use('vue-loader')
     //    .loader('vue-loader')
     //    .tap(options => {
     //      // 修改它的选项...
     //      return options
     //    })
     // 添加一个新的 Loader
      // GraphQL Loader
    // config.module
    //   .rule('graphql')
    //   .test(/\.graphql$/)
    //   .use('graphql-tag/loader')
    //     .loader('graphql-tag/loader')
    //     .end()
    // 替换一个规则里的 Loader
    //  const svgRule = config.module.rule('svg')
    // // 清除已有的所有 loader。
    // // 如果你不这样做，接下来的 loader 会附加在该规则现有的 loader 之后。
    // svgRule.uses.clear()
    // // 添加要替换的 loader
    // svgRule
    //   .use('vue-svg-loader')
    //     .loader('vue-svg-loader')
    // 修改插件选项
    // config
    //   .plugin('html')
    //   .tap(args => {
    //     return [/* 传递给 html-webpack-plugin's 构造函数的新参数 */]
    //   })
  }
}
// 在模板中，你首先需要向你的组件传入基础 URL：
// data () {
//   return {
//     publicPath: process.env.BASE_URL
//   }
// }
// 然后：
// <img :src="`${publicPath}my-image.png`">
// 
// 预处理器 ======================================
// # Sass
// npm install -D sass-loader node-sass
// # Less
// npm install -D less-loader less
// # Stylus
// npm install -D stylus-loader stylus
// 然后你就可以导入相应的文件类型，或在 *.vue 文件中这样来使用：
// <style lang="scss">
// $color: red;
// </style>
// 自动化导入===============
// 如果你想自动化导入文件 (用于颜色、变量、mixin……)，你可以使用 style-resources-loader(https://github.com/yenshih/style-resources-loader)。这里有一个关于 Stylus 的在每个单文件组件和 Stylus 文件中导入 ./src/styles/imports.styl 的例子：
// vue.config.js
// const path = require('path')
// module.exports = {
//   chainWebpack: config => {
//     const types = ['vue-modules', 'vue', 'normal-modules', 'normal']
//     types.forEach(type => addStyleResource(config.module.rule('stylus').oneOf(type)))
//   },
// }
// function addStyleResource (rule) {
//   rule.use('style-resource')
//     .loader('style-resources-loader')
//     .options({
//       patterns: [
//         path.resolve(__dirname, './src/styles/imports.styl'),
//       ],
//     })
// }

// CSS Modules==================
// 你可以通过 <style module> 以开箱即用的方式在 *.vue 文件中使用 CSS Modules。
// 如果想在 JavaScript 中作为 CSS Modules 导入 CSS 或其它预处理文件，该文件应该以 .module.(css|less|sass|scss|styl) 结尾：
// import styles from './foo.module.css'
// // 所有支持的预处理器都一样工作
// import sassStyles from './foo.module.scss'
// 如果你想去掉文件名中的 .module，可以设置 vue.config.js 中的 css.modules 为 true：
// // vue.config.js
// module.exports = {
//   css: {
//     modules: true
//   }
// }
// 如果你希望自定义生成的 CSS Modules 模块的类名，可以通过 vue.config.js 中的 css.loaderOptions.css 选项来实现。所有的 css-loader 选项在这里都是支持的，例如 localIdentName 和 camelCase：
// vue.config.js
// module.exports = {
//   css: {
//     loaderOptions: {
//       css: {
//         localIdentName: '[name]-[hash]',
//         camelCase: 'only'
//       }
//     }
//   }
// }
// 向预处理器 Loader 传递选项==================
// 有的时候你想要向 webpack 的预处理器 loader 传递选项。你可以使用 vue.config.js 中的 css.loaderOptions 选项。比如你可以这样向所有 Sass 样式传入共享的全局变量：
// vue.config.js
// module.exports = {
//   css: {
//     loaderOptions: {
//       // 给 sass-loader 传递选项
//       sass: {
//         // @/ 是 src/ 的别名
//         // 所以这里假设你有 `src/variables.scss` 这个文件
//         data: `@import "~@/variables.scss";`
//       }
//     }
//   }
// }
// Loader 可以通过 loaderOptions 配置，包括：
// css-loader
// postcss-loader
// sass-loader
// less-loader
// stylus-loader
// 提示

// 这样做比使用 chainWebpack 手动指定 loader 更推荐，因为这些选项需要应用在使用了相应 loader 的多个地方。



// vue inspect > output.js
// # 只审查第一条规则
// vue inspect module.rules.0
// vue inspect --rule vue
// vue inspect --plugin html
// vue inspect --rules
// vue inspect --plugins
// 
// 环境变量和模式 =====================
// 下列文件来指定环境变量：
// .env                # 在所有的环境中被载入
// .env.local          # 在所有的环境中被载入，但会被 git 忽略
// .env.[mode]         # 只在指定的模式中被载入
// .env.[mode].local   # 只在指定的模式中被载入，但会被 git 忽略

// 一个环境文件只包含环境变量的“键=值”对：
// FOO=bar
// VUE_APP_SECRET=secret

// 模式 ================
// 模式是 Vue CLI 项目中一个重要的概念。默认情况下，一个 Vue CLI 项目有三个模式：
// development 模式用于 vue-cli-service serve
// production 模式用于 vue-cli-service build 和 vue-cli-service test:e2e
// test 模式用于 vue-cli-service test:unit

// 你可以通过传递 --mode 选项参数为命令行覆写默认的模式。例如，如果你想要在构建命令中使用开发环境变量，请在你的 package.json 脚本中加入：
// "dev-build": "vue-cli-service build --mode development",
// 
// 示例：Staging 模式
// 假设我们有一个应用包含以下 .env 文件：
// VUE_APP_TITLE=My App
// 和 .env.staging 文件：
// NODE_ENV=production
// VUE_APP_TITLE=My App (staging)

// vue-cli-service build 会加载可能存在的 .env、.env.production 和 .env.production.local 文件然后构建出生产环境应用；
// vue-cli-service build --mode staging 会在 staging 模式下加载可能存在的 .env、.env.staging 和 .env.staging.local 文件然后构建出生产环境应用。
// 这两种情况下，根据 NODE_ENV，构建出的应用都是生产环境应用，但是在 staging 版本中，process.env.VUE_APP_TITLE 被覆写成了另一个值。
// 
// 在客户端侧代码中使用环境变量
// 只有以 VUE_APP_ 开头的变量会被 webpack.DefinePlugin 静态嵌入到客户端侧的包中。你可以在应用的代码中这样访问它们：
// console.log(process.env.VUE_APP_SECRET)
// 在构建过程中，process.env.VUE_APP_SECRET 将会被相应的值所取代。在 VUE_APP_SECRET=secret 的情况下，它会被替换为 "secret"。
// 除了 VUE_APP_* 变量之外，在你的应用代码中始终可用的还有两个特殊的变量：
// NODE_ENV - 会是 "development"、"production" 或 "test" 中的一个。具体的值取决于应用运行的模式。
// BASE_URL - 会和 vue.config.js 中的 publicPath 选项相符，即你的应用会部署到的基础路径。
// 你可以在 vue.config.js 文件中计算环境变量。它们仍然需要以 VUE_APP_ 前缀开头。这可以用于版本信息 process.env.VUE_APP_VERSION = require('./package.json').version。

// # 只在本地有效的变量
// 有的时候你可能有一些不应该提交到代码仓库中的变量，尤其是当你的项目托管在公共仓库时。这种情况下你应该使用一个 .env.local 文件取而代之。本地环境文件默认会被忽略，且出现在 .gitignore 中。
// .local 也可以加在指定模式的环境文件上，比如 .env.development.local 将会在 development 模式下被载入，且被 git 忽略。
// 
// 

// 单文件
// vue serve
// vue serve test.vue
// vue build test.vue

// 打包页面
// npx vue-cli-service build --target lib --name myLib  src/test.vue
// 注意 （wc\wc-async不兼容IE11以下的浏览器）
// npx vue-cli-service build --target wc --name myLib  src/*.vue
// npx vue-cli-service build --target wc-async --name myLib  src/*.vue

// 注册多个 Web Components 组件的包
// npx vue-cli-service build --target wc --name foo 'src/components/*.vue' 

// 异步 Web Components 组件(按需加载)
// vue-cli-service build --target wc-async --name foo 'src/components/*.vue'
// 
// 本地预览====================
// npm install -g serve
// # -s 参数的意思是将其架设在 Single-Page Application 模式下
// # 这个模式会处理即将提到的路由问题
// serve -s dist
// npx serve -d dist