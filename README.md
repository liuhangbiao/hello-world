# hello-world

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```


```
Command line instructions
You can also upload existing files from your computer using the instructions below.


Git global setup
git config --global user.name "道心"
git config --global user.email "liuhangbiaoo@gmail.com"

Create a new repository
git clone https://gitlab.com/liuhangbiao/hello-world.git
cd hello-world
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/liuhangbiao/hello-world.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/liuhangbiao/hello-world.git
git push -u origin --all
git push -u origin --tags



git remote add gitlib https://gitlab.com/liuhangbiao/hello-world.git
```